#!/bin/sh


# Configure Flathub repository
flatpak remote-add --no-gpg-verify flathub https://flathub.org/repo/flathub.flatpakrepo

# Install the Flatdeb example applications
flatpak install -y --noninteractive org.chromium.Chromium




